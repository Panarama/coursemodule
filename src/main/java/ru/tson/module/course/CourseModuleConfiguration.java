package ru.tson.module.course;

import org.springframework.context.annotation.ComponentScan;

@ComponentScan(basePackageClasses = {CourseModule.class})
public class CourseModuleConfiguration {
}

package ru.tson.module.course;


import net.rutini.core.module.BaseIRModule;
import net.rutini.core.module.EventManager;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import javax.annotation.PostConstruct;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.IOException;
import java.io.StringReader;
import java.util.HashMap;

@Component
public class CourseModule extends BaseIRModule {

    @Value("${course.repeat_minutes}")
    private int minute;

    @Value("${course.pairs}")
    private String pairs;

    @PostConstruct
    void init(){
        events = new EventManager("update");
    }

    public String getName() {
        return "Course Module";
    }

    public void run() {
        new Thread(new Runnable() {
            public void run() {
                while (true){
                    try {
                        HashMap<String, Float> course = getCourse();
                        HashMap<String, Float> result = new HashMap();
                        String[] split = pairs.split(",");
                        for (String key : course.keySet()) {
                            for (String pair : split) {
                                if(key.equals(pair))
                                    result.put(key, course.get(key));
                            }
                        }
                        events.notify(CourseModule.this, "update", true, result);

                    } catch (IOException e) {
                        e.printStackTrace();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    try {
                        Thread.sleep(60000*minute);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }

            }
        }).start();
    }



    HashMap<String, Float> getCourse() throws Exception {
        RestTemplate rest = new RestTemplate();
        ResponseEntity<String> entity = rest.getForEntity("http://www.nationalbank.kz/rss/rates_all.xml", String.class);
        
        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
        Document doc = dBuilder.parse(new InputSource(new StringReader(entity.getBody())));

        doc.getDocumentElement().normalize();

        NodeList items = doc.getElementsByTagName("item");
        HashMap<String, Float> pairs = new HashMap<String, Float>();

        for (int i = 0 ; i < items.getLength(); i++){
            Element element = (Element) items.item(i);

            pairs.put(element.getElementsByTagName("title").item(0).getTextContent(),
                    Float.parseFloat(element.getElementsByTagName("description").item(0).getTextContent()));
        }
        return pairs;
    }


    public void destroy() throws Exception {

    }

    public void afterPropertiesSet() throws Exception {

    }
}
